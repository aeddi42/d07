/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/14 16:36:52 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/15 21:29:54 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Array.class.cpp"
#include <iostream>
#include <cstdlib>

int		main(void) {

	unsigned int	i;
	Array<int>		a;
	Array<int>		b(3);

	std::cout << std::endl << "Init two int array:" << std::endl;
	std::cout << "A size = " << a.size() << std::endl;
	std::cout << "B size = " << b.size() << std::endl << std::endl;

	std::cout << "Filling B:" << std::endl;
	for (i = 0; i < b.size(); i++) {
		b[i] = std::rand() % 42;
		std::cout << "B[" << i << "] = " << b[i] << std::endl;
	}

	a = b;

	std::cout << std::endl << "A = B" << std::endl << std::endl;
	std::cout << "A size = " << a.size() << std::endl;
	std::cout << "B size = " << b.size() << std::endl << std::endl;

	std::cout << "Displaying A:" << std::endl;
	for (i = 0; i < a.size(); i++) {
		std::cout << "A[" << i << "] = " << a[i] << std::endl;
	}

	std::cout << std::endl << "Trying to access to the inexistant elem B[" << b.size() << "]: ";
	try {
		b[b.size()];
	}
	catch (std::exception & e) {
		std::cout << e.what() << std::endl;
	}

	std::cout << std::endl << "Modifying A:" << std::endl;
	for (i = 0; i < a.size(); i++) {
		a[i] = std::rand() % 42;
		std::cout << "A[" << i << "] = " << a[i] << std::endl;
	}

	std::cout << std::endl << "Displaying B:" << std::endl;
	for (i = 0; i < b.size(); i++) {
		std::cout << "B[" << i << "] = " << b[i] << std::endl;
	}

	Array<int>		c(b);
	std::cout << std::endl << "Create C from copying B: C(B)" << std::endl;
	std::cout << std::endl << "Displaying C:" << std::endl;
	for (i = 0; i < c.size(); i++) {
		std::cout << "C[" << i << "] = " << c[i] << std::endl;
	}

	std::cout << std::endl << "Modifying C:" << std::endl;
	for (i = 0; i < c.size(); i++) {
		c[i] = std::rand() % 42;
		std::cout << "C[" << i << "] = " << c[i] << std::endl;
	}

	std::cout << std::endl << "Displaying B:" << std::endl;
	for (i = 0; i < b.size(); i++) {
		std::cout << "B[" << i << "] = " << b[i] << std::endl;
	}
	std::cout << std::endl;

	return 0;
}
