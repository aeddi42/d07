/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Array.class.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/14 16:57:35 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/14 18:32:20 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Array.class.hpp"

template<typename T>
				Array<T>::Array(void) : _size(0) {
	this->_array = new T[this->_size];
}

template<typename T>
				Array<T>::Array(unsigned int i) : _size(i) {
	this->_array = new T[this->_size];
}

template<typename T>
				Array<T>::Array(Array const & src) {
	*this = src;
}

template<typename T>
Array<T>&		Array<T>::operator=(Array const & rhs) {

	unsigned int	i;

	this->_size = rhs.size();
	delete [] this->_array;
	this->_array = new T[this->_size];

	for (i = 0; i < this->_size; i++)
		this->_array[i] = rhs._array[i];

	return *this;
}

template<typename T>
T&				Array<T>::operator[](unsigned int i) {

	if (i >= this->_size)
		throw std::exception();
	else
		return this->_array[i];
}

template<typename T>
				Array<T>::~Array(void) {
	delete [] this->_array;
}

template<typename T>
unsigned int	Array<T>::size(void) const {
	return this->_size;
}
