/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Array.class.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/14 16:57:25 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/15 21:30:01 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARRAY_CLASS_HPP
# define ARRAY_CLASS_HPP

#include <stdexcept>

template<typename T>
class	Array {

	public:
		
						Array(void);
						Array(unsigned int i);
						Array(Array const & src);
		Array<T>&		operator=(Array const & rhs);
		T&				operator[](unsigned int i);
						~Array(void);

		unsigned int	size(void) const;	

	private:

		T				*_array;
		unsigned int	_size;

};

#endif /* !ARRAY_CLASS_HPP */
