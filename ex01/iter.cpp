/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iter.cpp  	                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/14 12:02:44 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/15 21:39:34 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

template<typename T>
void	iter(T a[], unsigned int i, void(*point)(T const &)) {

	unsigned int	i2;

	for (i2 = 0; i2 < i; i2++)
		point(a[i2]);
}

void	multiply_by_ten(int const & i) {

	std::cout << (i * 10) << std::endl;
}

void	uppercase_it(char const & c) {

	std::cout << static_cast<char>(c - 32) << std::endl;
}

int		main(void) {

	int				a[] = { 1, 2, 3 };
	char			a2[] = { 'a', 'b', 'c'};
	unsigned int 	i = 3;
	unsigned int 	i2;

	std::cout << std::endl << "Multiply by 10" << std::endl << "Before:" << std::endl;
	for (i2 = 0; i2 < i; i2++)
		std::cout << a[i2] << std::endl;
	std::cout << "After:" << std::endl;
	::iter(a, i, multiply_by_ten);

	std::cout << std::endl << "Uppercase" << std::endl << "Before:" << std::endl;
	for (i2 = 0; i2 < i; i2++)
		std::cout << a2[i2] << std::endl;
	std::cout << "After:" << std::endl;
	::iter(a2, i, uppercase_it);
	std::cout << std::endl; 

	return 0;
}
